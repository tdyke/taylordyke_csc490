using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Ball : MonoBehaviour {
	
	
	public Transform _t;
	
	public List<GameObject> blocks;
	// Use this for initialization
	
	void Awake() {
		_t = transform;	
		
		blocks = new List<GameObject>();
	}
	void Start () {
		blocks.AddRange(GameObject.FindGameObjectsWithTag("Brick"));
		rigidbody.velocity = Vector3.one * 0;
		
	}
	

	
	// Update is called once per frame
	void Update () {

		Vector3 v = rigidbody.velocity;
		Vector3 topRight = Camera.main.WorldToViewportPoint(transform.position + collider.bounds.extents);
		Vector3 bottomLeft = Camera.main.WorldToViewportPoint(transform.position - collider.bounds.extents);

		if (topRight.x > 1) {
			rigidbody.velocity = new Vector3(-Mathf.Abs(v.x), v.y, v.z);	
		}
		else if (topRight.y > 1) {
			rigidbody.velocity = new Vector3(v.x, -Mathf.Abs(v.y), v.z);
		}
		else if (bottomLeft.x < 0) {
			rigidbody.velocity = new Vector3(Mathf.Abs(v.x), v.y, v.z);	
		}
		
		if (bottomLeft.y < 0) {
			MissedBall();
		}
		
		if(Input.GetButtonDown("Fire1") || Input.GetKeyDown("space")){
			rigidbody.velocity = Vector3.one * 75;
		}
		

	}
		void OnCollisionEnter(Collision col) {

			if (col.gameObject.CompareTag("Brick"))
				blocks.Remove (col.gameObject);
		
		
	}
	
	
	private void MissedBall() {
		if (Lives.curLives-- < 1 /*|| blocks.Count < 1*/){
			Application.LoadLevel (0);
		}
		else
			ResetBall();
	}
	
	private void ResetBall() {
			transform.position = new Vector3(0,-84,0);
			rigidbody.velocity = Vector3.one * 0;
			if(Input.GetButtonDown("Fire1") || Input.GetKeyDown("space")){
				rigidbody.velocity = Vector3.one * 75;
	}
	
}	
}
	

