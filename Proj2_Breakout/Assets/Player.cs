using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	
	private float speed = 0;
	public static float zPosition = 0;
	public float yPosition = -90;
	private float xPosition = 0;
	
	private Ball ball;
	
	private Transform _t;
	
	void Awake() {
		_t = transform;	
	}

	// Use this for initialization
	void Start () {
		_t.position = new Vector3(0, yPosition, zPosition);
		ball = (Ball)FindObjectOfType(typeof(Ball));
	}
	
	// Update is called once per frame
	void Update () {
		
		if (ball.transform.position.y < -95) {
			_t.position = new Vector3(0, -90, 0);
			speed = 0;
			if (Input.GetButtonDown("Fire1") || Input.GetKeyDown("space")){
				speed = 100;
		}
		}
		
		Vector3 rightBoundry = Camera.main.WorldToViewportPoint(transform.position + collider.bounds.extents);
		Vector3 leftBoundry = Camera.main.WorldToViewportPoint(transform.position - collider.bounds.extents);
		
		if (Input.GetButtonDown("Fire1") || Input.GetKeyDown("space")){
			speed = 100;
	}
		
			
		
		xPosition = _t.position.x + Input.GetAxis("Horizontal") 
				* speed * Time.deltaTime;
		
	if (Input.GetAxis("Horizontal") !=0) {
			_t.position = new Vector3(xPosition, yPosition, zPosition);	
		}
		
		if(leftBoundry.x < 0)
			_t.position = new Vector3(xPosition+1.6f, yPosition, zPosition);
		if(rightBoundry.x > 1)
			_t.position = new Vector3(xPosition-1.6f, yPosition, zPosition);
		

}}
