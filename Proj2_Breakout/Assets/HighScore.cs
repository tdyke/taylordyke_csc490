using UnityEngine;
using System.Collections;

public class HighScore : MonoBehaviour {
	
	public static int highScore = 0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Score.score > PlayerPrefs.GetInt("highScore"))
			PlayerPrefs.SetInt("highScore", Score.score);
		
		guiText.text = "High Score: " + PlayerPrefs.GetInt("highScore");
	}
}
