using UnityEngine;
using System.Collections;

public class Lives : MonoBehaviour {
	public int startingLives = 3;
	public static int curLives;

	// Use this for initialization
	void Start () {
		curLives = startingLives;
	}
	
	// Update is called once per frame
	void Update () {
		guiText.text = "Lives: " + curLives.ToString();
	}
}
